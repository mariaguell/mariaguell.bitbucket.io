/* ┌───────────────────────────────────────────────────────────────┐
   │                          Informació                           │
   ├───────────────────────────────────────────────────────────────┤
   │ Maria Güell i Xavier Moll                                     │
   │ Entorns Interactius                                           │
   │ Pràctica 01: Dibuix Generatiu                                 │
   │ Enllaç remot de la pràctica: https://mariaguell.bitbucket.io/ │
   └───────────────────────────────────────────────────────────────┘ */

//definició de variables

let CANVAS_SIZE = 600;
let COLS = 8; //dividim el canvas en un nombre de files i columnes
let ROWS = 8;
let COL_WIDTH;
let ROW_HEIGHT;
let colors = [[229, 23, 23], [53, 69, 209], [240, 245, 102], [0, 0, 0], [255, 255, 255]]; //definim array dels possibles colors de cada forma
var formes = []; //creem un array on emmagatzemar cada forma

function setup() {
	createCanvas(CANVAS_SIZE, CANVAS_SIZE);
	frameRate(30);
	//rectMode(CENTER);
	COL_WIDTH = CANVAS_SIZE/COLS; //definim la amplada i alçada de les columnes i files
	ROW_HEIGHT = CANVAS_SIZE/ROWS;
}

function draw() {
	if (mouseIsPressed)
		formes.push(new Quadre(getCord(mouseX, mouseY), random(colors), formes)); //mentre es faixi clic es creen constanment objectes de la classe Quadre.Li passem al constructor la posició, un color aleatori i l'array de formes. El nou objecte es guarda dins formes.
	formes.forEach(f => {
		if (f.isActive) {
			//per cada objecte dins formes, es mira si es troba actiu i en ser així s'actualitza i es dibuixa
			f.update();
			f.display();
		}
	});
}

function getCord(posX, posY) {
	//a partir de les coordenades del mouse on s'ha fet clic, aproximem a quina fila i columna es dibuixarà el quadrat
	var c, r;
	c = posX/CANVAS_SIZE;
	c *= COLS;
	c = int(c);
	r = posY/CANVAS_SIZE;
	r *= ROWS;
	r = int(r);
	return [c, r]; //es retorna el nombre de la columna i fila, no la posició final al canvas
}

function deviceShaken() {//creem la funcio deviceShaken per capturar l'acceleracio del mobil (si n'hi ha)
    var posX = random(0, CANVAS_SIZE);//hem de decidir a on apereixera el poligon, com que no s'ha fet clic a cap posicio concreta, ens la inventem
    var posY = random(0, CANVAS_SIZE);
    formes.push(new Quadre(getCord(posX, posY), random(colors), formes));//i finalment,creem un nou pligon i l'afegim a la llista de poligons
}

function keyPressed() {
	if (key == 'c') {
		//si es prem la tecla 'c' (clear) es buida el canvas i l'array de formes
		formes = [];
		background(255);
	}
}

class Quadre {
	constructor(cords, color, array) {
		var c = cords[0];
		var r = cords[1];
		this.color = color;

		var w = int(random(1, 4)); //es decideixen aleatoriament les mides del quadre
		var h = int(random(1, 4));

		this.posX = c * COL_WIDTH; //calculam les coordenades al canvas
		this.posY = r * ROW_HEIGHT;
		this.width = w * COL_WIDTH; //calculem les mides reals, sempre multiples de COL_WIDTH/ROW_HEIGHT
		this.height = h * ROW_HEIGHT;

		this.isActive = true; //definim variable per aturar les actualitzacions quan s'ha acabat l'animació
		this.vel = 0.01; //definim la velocitat de l'animació
		this.array = array;
		this.currentSize = 0.001; //definim variable per controlar l'animació
		
		this.display();
	}
	update() {
		this.currentSize += this.vel; //a cada crida s'incrementa currentSize segons vel. Quan arriba a 1 s'aturen les actualizacions per aquest objecte
		if(this.currentSize >= 1){
			this.currentSize = 1;
			this.stop();
		}
	}
	display() {
		fill(this.color);
		strokeWeight(0);
		var w = this.width * this.currentSize; //s'augmenta gradualment la amplada i alçada de la forma per aconseguir un efecte de expansió
		var h = this.height * this.currentSize;
		rect(this.posX, this.posY, w, h);
	}
	generateTriangle() {//es crea un triangle adjacent al quadrat
		var rigtNotBottom = (random(100)>50); //es defineix si el triangle apareix a la dreta o abaix del triangle

		var p2x	=	this.posX + this.width; //es defineixen els punts del triangle a partir de la posició i la mida del quadrat
		var p2y	=	this.posY + this.height;
		var p1x	=	rigtNotBottom ? this.posX + this.width	: this.posX; //el p1 varia segons el triangle es troba a la dreta o abaix del triangle
		var p1y	=	rigtNotBottom ? this.posY				: this.posY + this.height;
		var d	=	this.height; //definim mida del triangle per controlar posteriorment l'animaciño

		this.array.push(new Triangle(p1x, p1y, p2x, p2y, d, this.color, rigtNotBottom)); //s'afegeix objecte de la classe Triangle dins l'array
	}
	stop() { //quan el quadre ha finalitzat l'animació es converteix en inactiu i s'inicia la generació d'un triangle
		this.isActive = false;
		if(this.width == this.height)
			this.generateTriangle();
	}
}
class Triangle {
	constructor(p1x, p1y, p2x, p2y, d, color, rigtNotBottom) {
		this.color = color;
		this.vel = 1;

		this.p1x = p1x;
		this.p1y = p1y;
		this.p2x = p2x;
		this.p2y = p2y;
		this.d = d;

		this.isActive = true;

		this.rigtNotBottom = rigtNotBottom;
		this.p3x = p2x; //definim el punt 3 igual al punt 2
		this.p3y = p2y;
	}
	update() {
		if(this.rigtNotBottom) { //si el triangle és a la dreta, el punt 3 es desplaça cap a la dreta 
			this.p3x += this.vel;
			if ((this.p3x - this.p2x) >= this.d)
				this.stop();
		}	
		else { //si el triangle és abaix, el punt 3 es desplaça cap avall
			this.p3y += this.vel;
			if ((this.p3y - this.p2y) >= this.d)
				this.stop();
		}
		
    }
    
	display() {
		fill(this.color);
		triangle(this.p1x, this.p1y, this.p2x, this.p2y, this.p3x, this.p3y); //es dibuixa el triangle a partir de tres punts
    }
    
	stop() {
		this.isActive = false;
    }
    
}
